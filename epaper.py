import sys
import os
import pyinotify
import logging

from epdlib import epd3in7
import time
from PIL import Image, ImageDraw, ImageFont
import traceback
import socket

logging.basicConfig(level=logging.DEBUG)


class ModifyEventHandler(pyinotify.ProcessEvent):
    '''File modify event class'''
    def process_IN_CLOSE_WRITE(self, event):
        if event.pathname == '/home/pi/repostit/memo/memo.txt':
            epaper.renew_epaper()
            #print(epaper.get_memo())


class EpaperHandler:
    '''epaper class'''
    def __init__(self):
        self.epd = epd3in7.EPD()
        self.font15 = ImageFont.truetype('epdlib/Font.ttc', 15)
        self.font24 = ImageFont.truetype('epdlib/Font.ttc', 24)


    def get_memo(self):
        path = os.path.dirname(__file__)
        try:
            with open(os.path.join(path, "memo/memo.txt")) as f:
                lines = []
                for line in f:
                    lines.append(line.rstrip())
                _memo = '\n'.join(lines)
        except:
            print('ohoh')
            _memo = ''
        return _memo


    def get_host_ip(self):
        ip = ''
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 80))
            ip = s.getsockname()[0]
        except:
            ip = 'Unknown'
        finally:
            s.close()

        return ip


    def reset(self):
        self.epd.reset()

    def renew_epaper(self):
        try:
            logging.info("epd3in7 Demo")

            logging.info("init and Clear")
            self.epd.init(0)
            self.epd.Clear(0xFF, 0)

            # partial update, just 1 Gary mode
            time_image = Image.new('1', (self.epd.height, self.epd.width), 255)
            time_draw = ImageDraw.Draw(time_image)

            txt = self.get_memo()
            time_draw.rectangle((10, 10, 120, 50), fill = 255)
            time_draw.text((10, 10), txt, font = self.font24, fill = 0)
            self.epd.display_1Gray(self.epd.getbuffer(time_image))

            #logging.info("Goto Sleep...")
            #self.epd.sleep()

        except IOError as e:
            logging.info(e)
            error_class = e.__class__.__name__ #取得錯誤類型
            detail = e.args[0] #取得詳細內容
            cl, exc, tb = sys.exc_info() #取得Call Stack
            lastCallStack = traceback.extract_tb(tb)[-1] #取得Call Stack的最後一筆資料
            fileName = lastCallStack[0] #取得發生的檔案名稱
            lineNum = lastCallStack[1] #取得發生的行號
            funcName = lastCallStack[2] #取得發生的函數名稱
            errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
            print(errMsg)
        except KeyboardInterrupt:
            logging.info("ctrl + c:")
            epd3in7.epdconfig.module_exit()

            exit()


if __name__ == '__main__':
    epaper = EpaperHandler()
    epaper.renew_epaper()
    time.sleep(1)
    pyinotify_flags = pyinotify.IN_CLOSE_WRITE

    # watch manager
    wm = pyinotify.WatchManager()
    watch_path = '/home/pi/repostit/memo/'
    wm.add_watch(watch_path, pyinotify_flags, rec=True)  # rec = recursive

    # event handler
    eh = ModifyEventHandler()

    # notifier
    notifier = pyinotify.Notifier(wm, eh)
    notifier.loop()

